// Package filetypes provides a function to identify supported audio file types, and decode audio files according to their format.
package filetype

import (
	"bytes"
	"fmt"
	"github.com/faiface/beep"
	"github.com/faiface/beep/flac"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/vorbis"
	"github.com/faiface/beep/wav"
	"os"
)

// Filetype describes the type of a supported audio file.
type FileType int

const (
	Flac FileType = iota + 1
	Mp3
	Vorbis
	Wav
)

// Identify returns a FileType if the file is one of the supported audio file types.
func Identify(path string) (FileType, error) {
	f, err := os.Open(path)
	if err != nil {
		return 0, fmt.Errorf("Error opening file %q:\n  %w", path, err)
	}
	defer f.Close()
	buf := make([]byte, 512)
	_, err = f.Read(buf)
	if err != nil {
		return 0, fmt.Errorf("Error reading file %q:\n  %w", path, err)
	}
	if bytes.Equal(buf[0:4], []byte{0x66, 0x4c, 0x61, 0x43}) {
		return Flac, nil
	} else if bytes.Equal(buf[0:3], []byte{0x49, 0x44, 0x33}) {
		return Mp3, nil
	} else if bytes.Equal(buf[0:4], []byte{0x4F, 0x67, 0x67, 0x53}) {
		return Vorbis, nil
	} else if bytes.Equal(buf[0:4], []byte{0x52, 0x49, 0x46, 0x46}) && bytes.Equal(buf[8:12], []byte{0x57, 0x41, 0x56, 0x45}) {
		return Wav, nil
	}
	return 0, nil
}

// Decode is the same as beep.[format].Decode, except it takes a path as an input, and automatically checks the file format to determine which package to use to open it.
func Decode(path string) (beep.StreamSeekCloser, beep.Format, error) {
	filetype, err := Identify(path)
	if err != nil {
		return nil, beep.Format{}, fmt.Errorf("Error identifying file type:\n  %w", path, err)
	}
	f, err := os.Open(path)
	if err != nil {
		return nil, beep.Format{}, fmt.Errorf("Error opening file %q:\n  %w", path, err)
	}
	switch filetype {
	case Flac:
		return flac.Decode(f)
	case Mp3:
		return mp3.Decode(f)
	case Vorbis:
		return vorbis.Decode(f)
	case Wav:
		return wav.Decode(f)
	}
	return nil, beep.Format{}, fmt.Errorf("File %q has unsupported format", path)
}
