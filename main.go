package main

import (
	"bytes"
	crand "crypto/rand"
	"encoding/binary"
	"fmt"
	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"gitlab.com/your_friend_alice/granular-synthesis-experiments/pkg/filetype"
	"log"
	"math"
	"math/rand"
	"os"
	"time"
)

func IMin(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func crossfade(ratio float64) float64 {
	return math.Cos(ratio * 0.5 * math.Pi)
}

type XFadeableStreamer struct {
	beep.StreamSeeker
	fadeLen int
}

func FadeInOut(stream beep.StreamSeeker, fade int) XFadeableStreamer {
	return XFadeableStreamer{stream, fade}
}

func (x XFadeableStreamer) Stream(samples [][2]float64) (int, bool) {
	startingPos := x.StreamSeeker.Position()
	n, ok := x.StreamSeeker.Stream(samples)
	for i := range samples {
		pos := startingPos + i
		if pos <= x.fadeLen+1 {
			// fade in
			for ch := range []int{0, 1} {
				samples[i][ch] *= crossfade(1 - float64(pos)/float64(x.fadeLen))
				_ = ch
			}
		}
		if pos >= x.StreamSeeker.Len()-x.fadeLen-1 {
			// fade out
			for ch := range []int{0, 1} {
				samples[i][ch] *= crossfade(float64(pos-x.Len()+x.fadeLen) / float64(x.fadeLen))
				_ = ch
			}
		}
	}
	return n, ok
}

type Shuffler struct {
	buf           beep.Buffer
	fragLen       int
	xFade         int
	prevStream    beep.StreamSeeker
	currentStream beep.StreamSeeker
}

func NewShuffler(buf beep.Buffer, fragment time.Duration, crossfade time.Duration) (*Shuffler, error) {
	if crossfade >= fragment/2 {
		return nil, fmt.Errorf("Crossfade must be less than half of fragment duration")
	}
	s := &Shuffler{
		buf:     buf,
		fragLen: int(fragment.Seconds() * float64(buf.Format().SampleRate)),
		xFade:   int(crossfade.Seconds() * float64(buf.Format().SampleRate)),
	}
	s.next()
	return s, nil
}

func (s *Shuffler) newPos() int {
	return rand.Intn(s.buf.Len() - s.fragLen - 1)
}

func (s *Shuffler) next() {
	p := s.newPos()
	s.prevStream = s.currentStream
	s.currentStream = FadeInOut(s.buf.Streamer(p, p+s.fragLen), s.xFade)
}

func (s *Shuffler) remaining() int {
	return s.currentStream.Len() - s.currentStream.Position()
}

func (s *Shuffler) Stream(samples [][2]float64) (int, bool) {
	pos := 0
	for pos < len(samples)-1 {
		if s.remaining() <= s.xFade {
			s.next()
		}
		lengthToAppend := IMin(s.remaining(), len(samples)-pos)
		for i := range samples {
			for ch := range samples[i] {
				samples[i][ch] = 0
			}
		}
		if s.prevStream != nil {
			s.prevStream.Stream(samples[pos : pos+lengthToAppend])
		}
		addBuf := make([][2]float64, lengthToAppend)
		s.currentStream.Stream(addBuf)
		for i := range addBuf {
			for ch := range []int{0, 2} {
				samples[i][ch] += addBuf[i][ch]
			}
		}
		pos += lengthToAppend
	}
	return len(samples), true
}

func (_ *Shuffler) Err() error {
	return nil
}

func Seed() {
	b := make([]byte, 8)
	_, err := crand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	var seed int64
	err = binary.Read(bytes.NewBuffer(b), binary.BigEndian, &seed)
	if err != nil {
		log.Fatal(err)
	}
	rand.Seed(seed)
}

func main() {
	Seed()
	if len(os.Args) != 4 {
		fmt.Println("Usage: " + os.Args[0] + " <crossfade duration> <fragment duration> <file>")
		os.Exit(1)
	}
	frag, err := time.ParseDuration(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	xFade, err := time.ParseDuration(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Loading")
	streamer, format, err := filetype.Decode(os.Args[3])
	if err != nil {
		log.Fatal(err)
	}
	buffer := beep.NewBuffer(format)
	buffer.Append(streamer)
	streamer.Close()
	shuffler, err := NewShuffler(*buffer, frag, xFade)
	if err != nil {
		log.Fatal(err)
	}

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	fmt.Println("Playing")
	speaker.Play(shuffler)
	select {}
}
